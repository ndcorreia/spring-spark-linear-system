package com.linearsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSparkLinearSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSparkLinearSystemApplication.class, args);
	}

}
